from flask import Flask
from flask import jsonify, request
from flask_pymongo import PyMongo
from flask_cors import CORS

from bson.json_util import dumps
from bson.objectid import ObjectId

from werkzeug.security import generate_password_hash, check_password_hash


app = Flask(__name__)

app.secret_key = "secretkey"

app.config['MONGO_URI'] = "mongodb+srv://spotify_user:spotify@spotify1.ock2m.mongodb.net/spotify_db?retryWrites=true&w=majority"

mongo = PyMongo(app)

@app.route('/add', methods=['POST'])
def add_artist():
    _json = request.json
    _name = _json["name"]
    _location = _json["location"]

    if _name and _location and request.method == 'POST':

        id = mongo.db.artist.insert({'name': _name, 'location': _location})

        resp = jsonify("Artist added successfully")
        resp.status_code = 200

        return resp

    else:
        return not_found()

@app.route('/artists')
def artists():
    artists = mongo.db.artist.find()
    resp = dumps(artists)
    return resp

@app.route('/artist/<id>')
def artist(id):
    artist = mongo.db.artist.find_one({'_id': ObjectId(id)})
    resp = dumps(artist)
    return resp

@app.route('/artist/update/<id>', methods=['PUT'])
def update_artist(id):
    _id = id
    _json = request.json
    _name = _json["name"]
    _location = _json["location"]

    if _id and _name and _location and request.method == "PUT":
        
        mongo.db.artist.update_one({'_id': ObjectId(_id['$oid']) if '$oid' in _id else ObjectId(_id)}, {'$set': {'name': _name, 'location': _location}})
        resp = jsonify("User updated successfully")
        resp.status_code = 200
        return resp
    
    else:
        return not_found()

@app.route('/artist/delete/<id>', methods=['DELETE'])
def delete_artist(id):
    mongo.db.artist.delete_one({'_id': ObjectId(id)})
    resp = jsonify("artist deleted successfully")
    resp.status_code = 200
    return resp




@app.errorhandler(404)
def not_found(error=None):
    message = {
        'status': 404,
        'message': 'Not found' + request.url
    }
    resp = jsonify(message)
    resp.status_code = 404

    return resp


if __name__ == '__main__':
    app.run(debug=True)